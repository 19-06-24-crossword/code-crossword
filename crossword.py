def check_down(list_grid: list, row: int, column: int) -> bool:
    max_down_range = len(list_grid) - 1
    if row - 1 < 0:
       return list_grid[row + 1][column] == "W"

    elif row == max_down_range:
        return  False

    else:
        return  list_grid[row - 1][column] == "B" and list_grid[row + 1][column] == "W"

def check_across(list_grid: list, row: int, column: int) -> bool:
    max_across_range = len(list_grid[0]) - 1

    if column - 1 < 0:
        return list_grid[row][column + 1] == "W"

    elif column  == max_across_range:
        return False

    else:
        return list_grid[row][column - 1] == "B" and list_grid[row][column + 1] == "W"


def numbering(list_grid):
    lst_output = []
    num = 0
    for row in range(len(list_grid)):
        for column in range(len(list_grid[0])):
            if list_grid[row][column] == "W":
                if check_down(list_grid, row, column) or check_across(list_grid, row,  column):
                    num += 1
                    lst_output.append([row, column, num])
    return lst_output

grid = [
 ["W", "W", "W", "W", "W", "W", "W", "W", "B", "W", "W", "W", "W", "W", "W"],
 ["W", "B", "W", "B", "W", "B", "W", "B", "B", "B", "W", "B", "W", "B", "W"],
 ["W", "W", "W", "W", "W", "W", "W", "W", "B", "W", "W", "W", "W", "W", "W"],
 ["W", "B", "W", "B", "W", "B", "W", "B", "B", "B", "W", "B", "W", "B", "W"],
 ["W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "B", "B", "B"],
 ["W", "B", "W", "B", "B", "B", "W", "B", "W", "B", "W", "B", "W", "B", "W"],
 ["W", "W", "W", "W", "W", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
 ["W", "B", "W", "B", "W", "B", "W", "B", "W", "B", "W", "B", "W", "B", "W"],
 ["W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "W", "W", "W", "W", "W"],
 ["W", "B", "W", "B", "W", "B", "W", "B", "W", "B", "W", "B", "W", "B", "W"],
 ["B", "B", "B", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
 ["W", "B", "W", "B", "W", "B", "W", "B", "W", "B", "W", "B", "W", "B", "W"],
 ["W", "W", "W", "W", "W", "W", "B", "W", "W", "W", "W", "W", "W", "W", "W"],
 ["W", "B", "W", "B", "W", "B", "W", "B", "W", "B", "W", "B", "W", "B", "W"],
 ["W", "W", "W", "W", "W", "W", "B", "W", "W", "W", "W", "W", "W", "W", "W"]
]
print(numbering(grid))
